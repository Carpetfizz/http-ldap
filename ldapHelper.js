var ldap = require('ldapjs');

/**
 * Creates LDAP client given a URL
 * 
 * @class
 * @constructor
 * @param {string} url - The fully qualified URL of your LDAP directory server
*/
function LdapHelper(url){
	this.client = ldap.createClient({
		url: url,
		timeout: 10000,
		connectTimeout: 10000
	});
}

/**
 * Binds http server to LDAP directory
 * 
 * @param {object} credentials Bind credentials of the user logging in
 * @param {string} credentials.bindDN The DN of the user logging in
 * @param {string} credentials.bindCredentials The bind credentials of the user logging in (password)
 * @param {bindCallback} callback - The callback that handles the bind operation
*/
LdapHelper.prototype.bind = function(credentials, callback){
	this.client.bind(credentials.bindDN, credentials.bindCredentials, function(err){
		if(err){
			callback({
				error: err,
				auth: false
			});
		}else{
			callback({
				auth: true
			});
		}
	});
}
/* UNTESTED */
/**
 * Unbinds the http server from the LDAP directory
 * 
 * @param {unbindCallback} callback - The callback that handles errors during unbind
 */
LdapHelper.prototype.unbind = function(callback) {
	this.client.unbind(function(err){
		if(err){
			callback(err);
		}
	});
}
/**
 * Searches the LDAP directory
 * 
 * @param {string} base - The Base DN of the search
 * @param {object} options - Options for the search operation
 * @param {string} options.scope - The scope of the search, "one", "sub", "base"
 * @param {string} options.filter - The LDAP search feature with enclosing parenthesis
 * @param {array} options.attributes - The attributes to return with the search
 * @param {boolean} options.attrsOnly - The server will only return the attribute names and not their values
 * @param {int} options.sizeLimit - The number of entries to return
 * @param {int} options.timeLimit - The max seconds the server can take to respond
 * @param {searchCallback} callback - The callback that handles the results of the search operation
 */
LdapHelper.prototype.search = function(base, options, callback){
	this.client.search(base, options, function(err, res){
		var results = [];
		if(res){
			res.on('searchEntry', function(entry) {
				results.push(entry.object);
			});
			res.on('searchReference', function(referral) {
				callback(referral.uris.join(), null);
			});
			res.on('error', function(err){
				callback(null, err);
			});
			res.on('end', function(result){
				callback(results, null);
			});
		}
	});
}

LdapHelper.prototype.compare = function(dn, attribute, value, callback) {
	this.client.compare(dn, attribute, value, function(err, matched){
		callback({
			error: err,
			matched: matched
		});
	});
}

module.exports = LdapHelper;

/**
 * Callback for unbind errors
 * 
 * @callback unbindCallback
 * @param {object} error
*/

/**
 * Callback for bind operation
 * 
 * @callback bindCallback
 * @param {object} status - An object with the status of the bind operation
 * @param {boolean} status.auth - True means the client is authenticated, false means it is not
 * @param {object} status.error - Object with the error of an unsuccessful bind
* /

/** 
 * Callback for search operation
 * 
 * @callback searchCallback
 * @param {array} results - An array of all the entries returned by the search operation
 * @param {object} error - Any error that may have occurred during the search operation
 */