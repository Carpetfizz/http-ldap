# http-ldap
Query an LDAP directory through HTTP requests

## How it works

http-ldap is an HTTP server that acts as an LDAP directory client. LDAP filters will be made by the server on behalf of the REST client. http-ldap will bind to a user specified in `config.json` at startup. Subsequent queries will be authenticated through that user.

## TODO

- [ ] SSL, https, ldaps support and testing
- [ ] Support for more idempotent(?) LDAP operations
- [ ] Test scripts

# Installation

http-ldap uses [npm](https://www.npmjs.com/), Node Package Manager. After cloning this repository, simply run the following two commands:

1. `npm install` will install all dependencies specified in `package.json`
2. `npm server.js` will start the express server

## Configuration

You'll find `config.example.json` provided in the repository. Rename this file to `config.json` and fill in the following configuration information.

```json
{
	"url": "ldap://YOUR_ADDRESS:1389",
	"credentials": {
		"bindDN": "YOUR_BIND_DN",
		"bindCredentials": "YOUR_BIND_CREDENTIALS"
	}
}
```

# API Reference

The API currently only supports the LDAP search operation. More to come soon.

## POST /search

Searching with a base DN, filter, and scope, will return an array of matching entries from the directory. Please refer to the [LdapJS](http://ldapjs.org/client.html)'s "search" guide for a more detailed explanation of each parameter.

Parameter     | Example
------------- | -------------
dn  | "ou=People, dc=example, dc=com"
filter  | "(&(mail=*@domain.net)(l=California))"
scope | "sub" or "one" or "base"
attributes | ['cn','sn','givenName','mail']
attrsOnly | true or false (defaults to false)
sizeLimit | 100
timeLimit | 5





LDAP login
env_ldap_url=ldap://sc9-ad-vip.vmware.com:3268/DC=vmware,DC=com
env_ldap_userDn=CN=SCA Workbench Portal,OU=Service Accounts,OU=Ops,DC=vmware,DC=com
env_ldap_password=A7Y@Use.e5y.Y.u.y!U

ldap_username: wsadmin@vmware.com


url: ldap://wdc-ad-vip.vmware.com:389
bindDn:
If you are using uid, then uid= wsadmin@vmware.com
If you are using cn, then cn= wsadmin
bindCredentials: welcome1
searchBase: ou=service_accounts,ou=people,dc=vmware,dc=com


 url: 'ldap://ora-dev15-fmidm-v1.vmware.com:6501',
            bindDn: 'uid=wsadmin@vmware.com,ou=service_accounts,ou=people,dc=vmware,dc=com',
            username : "wsadmin@vmware.com",
            password : 'welcome1'   
ldap://wdc-ad-vip.vmware.com:3268',
            'dc=vmware,dc=com',
            'admnptl-nonprod',
            'nY^A@A^Y7E!u@E.A@e.');


