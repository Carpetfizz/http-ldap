var express = require('express'),
	bodyParser = require('body-parser'),
	LdapHelper = require('./ldapHelper'),
	fs = require('fs'),
	app = express();

app.use(bodyParser.json({strict: false}));
app.use(bodyParser.urlencoded({extended: true}));

var config = JSON.parse(fs.readFileSync('config.json', 'utf8'));

/* Create the LDAP Client. URL must be in ldap://address:port format */


var lh = new LdapHelper(config.url);

lh.bind(config.credentials, function(status){
	if(status.auth){
		console.log("Successfully authenticated: "+config.credentials.bindDN);
	}else{
		console.error(status.error);
	}
});

/* HTTP POST
	dn: location of the root of the search, ex: 'ou=People, dc=example, dc=com'
	filter: fully qualified parenthetical LDAP filter, ex: '(&(mail=*@domain.net)(l=California))'
	scope: how deep to search, ex: 'base' , 'one', or 'sub'
	Returns [] of search results
*/

app.post('/search', function(req,res){
	var opts,
		base;
	opts = {
		scope: req.body.scope,
		filter: req.body.filter,
		attributes: req.body.attributes,
		attrsOnly: req.body.attrsOnly,
		sizeLimit: req.body.sizeLimit,
		timeLimit: req.body.timeLimit
	}
	base = req.body.base;
	lh.search(base,opts, function(object, error){
		if(!error){
			res.json(object);	
		}
	});
});

/* Start Express Server */
var server = app.listen(3000, function(){
	console.log('Server listening at %s', server.address().port);
});
